package electricwave;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;

public class ElecMain extends BasicGame{
	ArrayList<Circle> trail;
	WaveBall ball;
	public ElecMain(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void init(GameContainer container) throws SlickException {
		ball = new WaveBall(4, 5, 6);
		trail = new ArrayList<Circle>();
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		ball.draw(g);
		// TODO Auto-generated method stub
		for (Circle circle : trail){
			g.draw(circle);
		}
		
	}

	

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		ball.update(delta);
		trail.add(new Circle(5*ball.x + 300, 5*ball.z + 300, 1));
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new ElecMain("SwirlyBall"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
