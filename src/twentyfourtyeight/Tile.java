package twentyfourtyeight;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Tile {
	Rectangle rectangle;
	int data;
	int x;
	int y;
	
	public Tile(int x, int y, int data){
		this.x = x;
		this.y = y;
		this.data = data;
		rectangle = new Rectangle(x * 50, y * 50, 50, 50);
	}
	
	public void update(){
		
	}
	
	public void render(Graphics g){
		
	}

}
