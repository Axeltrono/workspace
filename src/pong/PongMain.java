package pong;

import java.io.IOException;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;


public class PongMain extends BasicGame{

	public PongMain(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	Audio music;
	LeftPaddle leftPaddle;
	RightPaddle rightPaddle;
	PongBall ball;
	int countDownTimer = -250;
	String time;
	
	@Override
	public void init(GameContainer container) throws SlickException {
		// TODO Auto-generated method stub
		
		try {
			music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("skyrim.wav"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		leftPaddle = new LeftPaddle(100);
		rightPaddle = new RightPaddle(100);
		try {
			ball = new PongBall(800/2, 600/2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		rightPaddle.draw(g);
		leftPaddle.draw(g);
		ball.draw(g);
		
		g.drawString("PLAYER 1: " + ball.player1Score, 200, 100);
		g.drawString("PLAYER 2: " + ball.player2Score, 480, 100);
		g.drawLine(400, 0, 400, 600);
		
		if (Math.abs(rightPaddle.velocity) - Math.abs(ball.velocityX) != 2 && Math.abs(leftPaddle.velocity) - Math.abs(ball.velocityX) != 2) {
			//sg.drawString("JIGUHMAJOO", 350, 400);
		}
		
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		
		if (!((ball.player1Score >= 6 && ball.player2Score >= 6) && Math.abs(ball.player1Score - ball.player2Score) <= 2)) {
			music.playAsMusic(1.0f, 1.0f, true);

		}
		System.out.println((ball.player1Score >= 6 && ball.player2Score >= 6) && Math.abs(ball.player1Score - ball.player2Score) <= 2);
		
		leftPaddle.update();
		rightPaddle.update();
		leftPaddle.ballCollider(ball);
		rightPaddle.ballCollider(ball);
		ball.update(leftPaddle, rightPaddle);

		
		
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new PongMain("Pong broe!"));
			window.setDisplayMode(800, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
