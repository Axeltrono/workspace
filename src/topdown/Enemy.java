package topdown;

import java.io.IOException;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Enemy {
	int x;
	int y;
	Circle circle;
	int health;
	boolean isDead = false;
	Audio death;
	Audio hurt;


	
	public Enemy(int x, int y, int health) throws IOException{
		death = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("explosion.wav"));
		hurt = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("hurt2.wav"));
		this.health = health;
		this.x = x;
		this.y = y;
		circle = new Circle(x, y, 10);
	}
	
	public void update(){
		if (!isDead) {
			y++;
			circle = new Circle(x, y, 10);
		}
		
	}
	
	public void render(Graphics g){
		if (!isDead) {
			g.draw(circle);
		}
	}
	
	public double getDistance(int bulletx, int bullety){
		return Math.sqrt(((y - bullety) * (y - bullety)) + ((x - bulletx) * (x - bulletx)));
	}
	
	public void playDeathSound(){
		death.playAsSoundEffect(1.0f, 1.0f, false);
	}
	
	public void playHurtSound(){
		hurt.playAsSoundEffect(1.0f, 1.3f, false);
	}
	

}
