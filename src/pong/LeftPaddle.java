package pong;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class LeftPaddle {
	int y;
	int velocity = 5;
	int height;
	
	public LeftPaddle(int height){
		y = 350;
		this.height = height;
	}
	
	public void update(){
		if (y + height < 600 && Keyboard.isKeyDown(Keyboard.KEY_S)) {
			y+=velocity;
		}
		if (y > 0 && Keyboard.isKeyDown(Keyboard.KEY_W)){
			y-=velocity;
		}
	}
	
	public void draw(Graphics g){
		g.draw(new Rectangle(0, y, 25, height));
	}
	
	public void ballCollider(PongBall ball){
		if (ball.y > y + height && ball.y < y) {
			if (ball.x - 15 < 25) {
				ball.velocityX = 5;
			}
		} 
	}

}
