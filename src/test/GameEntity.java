package test;

import org.newdawn.slick.Graphics;

public abstract class GameEntity {
	int counter = 0;
	int x;
	int y;
	public abstract void draw(Graphics g);
	public abstract void move(int delta);

}
