package gravity;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Background {
	Image image;
	int patternWidth;
	int patternHeight;
	public float patternPositionX;
	public float patternPositionY;
	public float tileCountX;
	public float tileCountY;
	public boolean isMoving = true;
	
	public Background(GameContainer container, int x, int y) throws SlickException{
		patternPositionX = x;
		patternPositionY = y;
		tileCountX = 1;
		tileCountY = 1;
		image = new Image("flappy.png");
		patternWidth  = container.getWidth();
		patternHeight = container.getHeight();
	}
	
	public void scrollX(){
		image.bind();
		if(isMoving){
			patternPositionX--;
		}
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0);
		GL11.glVertex2f(patternPositionX, patternPositionY);
		GL11.glTexCoord2f(tileCountX, 0);
		GL11.glVertex2f(patternWidth+patternPositionX, patternPositionY);
		GL11.glTexCoord2f(tileCountX, tileCountY);
		GL11.glVertex2f(patternWidth+patternPositionX, patternHeight+patternPositionY);
		GL11.glTexCoord2f(0, tileCountY);
		GL11.glVertex2f(patternPositionX, patternHeight+patternPositionY);
		GL11.glEnd();
		
	}
	
	public void scrollY(){
		image.bind();
		patternPositionY++;
		
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glTexCoord2f(0, 0);
		GL11.glVertex2f(patternPositionX, patternPositionY);
		GL11.glTexCoord2f(tileCountX, 0);
		GL11.glVertex2f(patternWidth+patternPositionX, patternPositionY);
		GL11.glTexCoord2f(tileCountX, tileCountY);
		GL11.glVertex2f(patternWidth+patternPositionX, patternHeight+patternPositionY);
		GL11.glTexCoord2f(0, tileCountY);
		GL11.glVertex2f(patternPositionX, patternHeight+patternPositionY);
		GL11.glEnd();
		
	}
	

}
