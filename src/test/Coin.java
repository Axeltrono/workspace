package test;

import java.io.IOException;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Coin extends GameEntity{
	 
	 Audio coinGet;
	 Circle circle;
	 Image coinimg;
	 Image coinimg2;
	 boolean hasBeenGot = false;
	
	public Coin(int x, int y) throws SlickException, IOException{
		coinGet = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("coin.wav"));
		this.x = x;
		coinimg = new Image("pitbullenjoyinglife.png");
		coinimg2 = new Image("pitbullenjoyinglife.png");
		this.y = y;
		circle = new Circle(x, y, 5);
	}
	
	public void update(Ball ball){
		if (getDistance(ball.x, ball.y) < 15) {
			coinGet.playAsSoundEffect(1.0f, 1.0f, false);
			hasBeenGot = true;
		}
		counter++;
		
	}
	@Override
	public void draw(Graphics g){
		if (!hasBeenGot) {
			
			if (counter < 30) {
				coinimg.getScaledCopy(40, 40).draw(this.x - 20, this.y - 20);
			} else if (counter > 30 && counter < 60) {
				coinimg2.getScaledCopy(40, 40).draw(this.x - 20, this.y - 20);
			} else if (counter > 30){
				coinimg.getScaledCopy(40, 40).draw(this.x - 20, this.y - 20);
				counter = 0;
			}
		} else {
			
		}
	}
	
	public double getDistance(int playerx, int playery){
		return Math.sqrt(((y - playery) * (y - playery)) + ((x - playerx) * (x - playerx)));
	}


	@Override
	public void move(int delta) {
		if (counter < 30){
			x++;
			
		} else {
			x--;
			
		}
	}

}
