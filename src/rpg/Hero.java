package rpg;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Hero {
	int x;
	int y;
	boolean isDead = false;
	Rectangle rect;
	boolean leftOrRight = true;
	Rectangle sword;
	float swingTime = 0;
	boolean canStrike = true;
	float swingMax = 1;
	

	public Hero(int x, int y) {
		this.x = x;
		this.y = y;
		sword = new Rectangle(800, 800, 0, 0);
		rect = new Rectangle(x, y, 25, 25);
	}

	public void render(Graphics g) {
		if (!isDead) {
			g.draw(rect);
			g.draw(sword);
		}
	}

	public void update(int delta) {
		if (!isDead) {
			if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
				y -= 2;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
				y += 2;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
				x -= 2;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
				x += 2;
			}
			
			
			// /new code
			swingTime += delta; // add to the swing time each update

			if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				if (canStrike) {
					sword = new Rectangle(x - 35, y + 5, 35, 10);
					// do what you want here when they hit the key
				} 

				// no clue why this has to be negated; dumbest method ever IMO
				if (!Keyboard.getEventKeyState()) {
					// able to strike while swingTime is less than the max time.
					if (swingTime < swingMax) {
						canStrike = true;
					}
					// otherwise no dice!
					else {
						canStrike = false;						
					}
				}
			}
			// if key isn't pressed, might as well reset everything
			else {
				swingTime = 0;
				canStrike = true;
				sword = new Rectangle(800, 800, 0, 0);
			}
			// end new code

			/*
			if (counter > 20) {
				if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
					sword = new Rectangle(x + 25, y + 5, 35, 10);
					counter = 0;
				} else if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
					
				} else {
				}

			} else {
			}
			*/

		}

		rect = new Rectangle(x, y, 25, 25);
	}

}
