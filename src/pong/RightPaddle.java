package pong;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class RightPaddle {
	int y;
	int velocity = 5;
	int height;
	
	
	public RightPaddle(int height){
		y = 350;
		this.height = height;
	}
	
	public void update(){
		if (y + height < 600 && Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			y+=velocity;
		}
		if (y > 0 && Keyboard.isKeyDown(Keyboard.KEY_UP)){
			y-=velocity;
		}
	}
	
	public void draw(Graphics g){
		g.draw(new Rectangle(775, y, 25, height));
	}
	
	public void ballCollider(PongBall ball){
		if (ball.y > y + height && ball.y < y) {
			if (ball.x + 15 > 775) {
				ball.velocityX = -5;
			}
		} 
	}

}
