package gamestatetut;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;

public class GameStateMain extends BasicGame{
	private static SwagState state = SwagState.INTRO;
	Image basedGod;
	int counter = 0;


	public GameStateMain(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		basedGod = new Image("lilb.png");
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void render(GameContainer container, Graphics g)	throws SlickException {
		switch (state) {
		case INTRO:
			g.draw(new Circle(300, 300, 10));
			break;
		case GAME:
			g.draw(new Rectangle(300, 300, 10, 10));
			break;
		case MAIN_MENU:
			basedGod.getScaledCopy(30, 30).draw(300, 300);
			break;
		}
		
		//while (Keyboard.next()) {
			if(Keyboard.isKeyDown(Keyboard.KEY_SPACE) && counter > 5) {
				if(state == SwagState.INTRO)
					state = SwagState.MAIN_MENU;
				else if(state == SwagState.MAIN_MENU)
					state = SwagState.GAME;
				else if(state == SwagState.GAME)
					state = SwagState.INTRO;
				counter = 0;
			}
		//}
			counter++;


	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new GameStateMain("BASEY GOD"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
