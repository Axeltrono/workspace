package rpg;

import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Enemy {
	boolean isDead = false;
	int x;
	int y;
	Rectangle rect;
	Random r = new Random();
	
	public Enemy(int x, int y){
		rect = new Rectangle(x, y, 25, 25);
		this.x = x;
		this.y = y;
	}
	
	public void update(Hero hero){
		if (!isDead) {
			if (hero.y > y) {
				y += r.nextInt(3);
				//x += r.nextInt(3);
			} else {
				y -= r.nextInt(3);
				//x -= r.nextInt(3);
			}
			if (hero.x > x) {
				x += r.nextInt(3);
				//y += r.nextInt(3);
			} else {
				x -= r.nextInt(3);
				//y -= r.nextInt(3);
			}
			if (rect.intersects(hero.sword)) {
				isDead = true;
			}
			if (rect.intersects(hero.rect) && !isDead) {
				hero.isDead = true;
			}
			
			
			
			rect = new Rectangle(x, y, 25, 25);
		}
	}
	
	public void draw(Graphics g){
		if (!isDead) {
			g.draw(rect);
		}
	}
}
