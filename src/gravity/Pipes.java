package gravity;

import java.io.IOException;
import java.util.Random;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Pipes {
	Image basedPipe;
	Rectangle top;
	Rectangle bottom;
	int height;
	int startingX;
	int x;
	Random r = new Random();
	Audio ding;
	boolean isMoving = true;
	
	public Pipes(int startingX) throws IOException, SlickException{
		basedPipe = new Image("thankyoubasedgod.png");
		ding = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("coinget.wav"));
		this.startingX = startingX;
		x = this.startingX;
		height = r.nextInt(450);
		top = new Rectangle(startingX, 0, 60, height);
		bottom = new Rectangle(startingX, 150 + height, 60, 600);
		
	}
	
	public void update(BouncyBall ball){
		if(isMoving){
			x-=5;
		}
		top = new Rectangle(x, 0, 60, height);
		bottom = new Rectangle(x, 150 + height, 60, 600);
		if (x + 60 < 0) {
			height = r.nextInt(450);
			x = 600;
		}
		//testing ball hitbox
		if (((ball.x + 10 > top.getMinX() && ball.x - 10 < top.getMaxX()) && (ball.y + 10 > bottom.getMinY() || ball.y - 10 < top.getMaxY())) || ball.y + 10 >= 555) {
			ball.isBallDead = true;
		}
		//
		if (ball.x == top.getCenterX()){
			if (!ball.isBallDead) {
				ding.playAsSoundEffect(1.0f, 1.0f, false);
				ball.score++;
			}	
		}
	}
	public void render(Graphics g){
		basedPipe.getScaledCopy(60, height).draw(x, 0);
		basedPipe.getScaledCopy(60, (int) (600 - bottom.getMinY())).draw(x, height + 150);
		g.draw(top);
		g.draw(bottom);
	}

}
