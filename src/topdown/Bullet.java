package topdown;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;

public class Bullet {
	int x;
	int y;
	Circle circle;
	String direction;
	
	public Bullet(String direction, int x, int y){
		this.x = x;
		this.y = y;
		this.direction = direction;
		circle = new Circle(x, y, 3);
	}
	
	public void update(){
		if(direction.equals("up")){
			y-=10;
		}
		if(direction.equals("down")){
			y+=10;
		}
		if(direction.equals("left")){
			x-=10;
		}
		if(direction.equals("right")){
			x+=10;
		}
		
		
		
		circle = new Circle(x, y, 3);
	}
	
	public void render(Graphics g){
		g.draw(circle);
	}

}
