package gravity;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class LoseMenu {
	Rectangle rect;
	Image menu;
	
	public LoseMenu() throws SlickException{
		rect = new Rectangle(600-502, 175, 100, 300);
		menu = new Image("losemenu.png");
	}
	
	public void render(Graphics g){
		menu.getScaledCopy(200, 400).draw(175, 50);
		
	}

}
