package test;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

public class Platform extends GameEntity{
	private Rectangle rect;
	boolean isBroken = false;
	
	public Platform(int x, int y, int width, int height){
		rect = new Rectangle(x, y, width, height);
	}
	

	@Override
	public void draw(Graphics g) {
		g.draw(rect);
		
	}
	
	@Override
	public void move(int delta) {
		// TODO Auto-generated method stub
		//no move here Broskiezzle
		
	}
	
	public boolean update(Ball ball){
		//System.out.println(rect.getMaxX());
		//System.out.println(rect.getY());
		if (((ball.x > (int) rect.getX() && ball.x < (int) rect.getX() + 25) && (ball.y + 10 >= (int) rect.getMinY() && ball.y <= rect.getMaxY()))){
			// ball stops moving down
			ball.y = (int) rect.getMinY() - 10;
			System.out.println("gravity should go to 0");
			ball.gravity = 0;
			ball.velocity = 0;
			ball.ballCanJump = true;
			isBroken = true;
			return true;
		} else {
			ball.gravity = 1;
			ball.ballCanJump = false;
			return false;
		}
		
	}

}
