package rpg;

import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class RPGMain extends BasicGame {

	public RPGMain(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	RPGGameState state = RPGGameState.GAME;
	Hero hero;
	Enemy enemy;
	ArrayList<Enemy> enemies = new ArrayList<Enemy>();
	boolean debug = true;
	Random r;

	@Override
	public void init(GameContainer container) throws SlickException {
		// TODO Auto-generated method stub
		r = new Random();
		for (int i = 0; i < 10; i++) {
			enemies.add(new Enemy(r.nextInt(600), r.nextInt(600)));
		}
		hero = new Hero(300, 300);
		enemy = new Enemy(400, 400);
	}

	@Override
	public void render(GameContainer container, Graphics g)
			throws SlickException {
		switch (state) {
		case GAME:
			enemy.draw(g);
			for (Enemy bro : enemies) {
				bro.draw(g);
			}
			hero.render(g);
			break;
		case MAIN_MENU:
			// make start screen
			break;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void update(GameContainer container, int delta)
			throws SlickException {
		hero.update(delta);
		enemy.update(hero);

		for (Enemy bro : enemies) {
			bro.update(hero);
			if (debug && Keyboard.isKeyDown(Keyboard.KEY_R)) {
				bro.isDead = false;
			}
		}
		if (debug && Keyboard.isKeyDown(Keyboard.KEY_R)) {
			enemy.isDead = false;
			hero.isDead = false;
		}

		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new RPGMain(
					"RPG TEST"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
