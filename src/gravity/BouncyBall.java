package gravity;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;

public class BouncyBall {
	Circle circle;
	int x;
	int y;
	int velocity = 0;
	double airdrag = .9;
	int score = 0;
	boolean isBallDead = false;
	GravityMain game;
	Image basedGod;
	
	public BouncyBall(GravityMain game) throws SlickException{
		basedGod = new Image("lilb.png");
		x = 200;
		y = 100;
		circle = new Circle(x, y, 10);
		this.game = game;
	}
	
	public void update(int delta){
		if(!isBallDead){
			//if (velocity <= 10){
				velocity++;
			//}
			
			if (y + 10 < 600) {
				y += velocity;
			} else {
				velocity = 0;
				y += velocity;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_UP) && y - 10 >= 0) {
				velocity = -6;
				y += velocity;
			}	
			if (y + 10 > 555) {
				y = 555;
			}
			circle = new Circle(x, y, 10);
		}	
	}
	
	public void render(Graphics g){
		basedGod.getScaledCopy(40, 40).draw(x - 20, y - 15);
		if (!isBallDead) {
			g.drawString(score + "", x - 7, y - 30);
			//g.draw(circle);
		} else {
			//this.x-=5;
		}	
	}
}
