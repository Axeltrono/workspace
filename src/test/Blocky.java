package test;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Line;

public class Blocky {
	int width;
	int height;
	int posx;
	int posy;
	Line line1;
	Line line2;
	Line line3;
	Line line4;
	
	public Blocky(int width, int height, int posx, int posy) {
		this.width = width;
		this.height = height;
		this.posx = posx;
		this.posy = posy;
		
		line1 = new Line(posx, posy, posx + width, posy);
		line2 = new Line(posx, posy, posx, posy + height);
		line3 = new Line(posx, posy + height, posx + width, posy + height);
		line4 = new Line(posx + width, posy, posx + width, posy + height);
		
	}
	
	public void draw(Graphics g){
		g.draw(line1);
		g.draw(line2);
		g.draw(line3);
		g.draw(line4);
	}

}
