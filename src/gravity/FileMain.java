package gravity;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FileMain {
	
	 public File file;
	 public Scanner scanner;

	 public FileMain(String path) {
		try {
		 	file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }

	 public void write(String str) {
		try {
		 	FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(str);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	 }
	 
	 public int read(File fileToRead) throws FileNotFoundException{
		 scanner = new Scanner(fileToRead);
		 return scanner.nextInt();
	 }

	

}
