package test;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class Ball extends GameEntity {
	boolean ballCanJump = false;
	public int gravity = 1;
	Circle circ;
	int movement = 2;
	int counterMax = 40;
	int jumper = 1;
	int velocity = 0;
	Image playerUp;
	Image playerUp2;
	Image playerRight;
	Image playerRight2;
	Image playerDown;
	Image playerDown2;
	Image playerLeft;
	Image playerLeft2;
	Image playerUpRight;
	Image playerDownLeft;
	Image playerDownRight;
	Image playerUpLeft;
	int jumpCounter = 0;
	Audio jump;
	boolean isOnPlatform = false;

	public Ball(int x, int y) throws SlickException, IOException {
		this.x = x;
		this.y = y;
		jump = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("jump.wav"));
		circ = new Circle(x, y, 10);
		playerUp = new Image("personup.png");
		playerUp2 = new Image("personup2.png");
		playerRight = new Image("personright.png");
		playerRight2 = new Image("personright2.png");
		playerDown = new Image("persondown.png");
		playerDown2 = new Image("persondown2.png");
		playerLeft = new Image("personleft.png");
		playerLeft2 = new Image("personleft2.png");
		playerUpRight = new Image("personupright.png");
		playerUpLeft = new Image("personupleft.png");
		playerDownLeft = new Image("persondownleft.png");
		playerDownRight = new Image("persondownright.png");

	}

	public void move(int delta) {
		velocity+=gravity;
		if (y + 10 < 600) {
			y += velocity;
		} else {
			velocity = 0;
			y += velocity;
		}
		if (y + 10 > 600) {
			y = 590;
		}

		if (jumpCounter > 40 && (ballCanJump || y + 10 >= 600)) {
			if (Keyboard.isKeyDown(Keyboard.KEY_UP) && (y - 10 > 0)) {
				velocity = -2 * jumper - 10;
				jump.playAsSoundEffect(1, 1, false);
				y += velocity;
				playerUp.getScaledCopy(20, 20).draw(this.x - 10, this.y - 10);
				jumpCounter = 0;
			}
		} else {
			jumpCounter++;
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN) && (y + 10 < 600)) {
			y += movement;
			playerDown.getScaledCopy(20, 20).draw(this.x - 10, this.y - 10);

		}

		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT) && (x + 10 < 600)) {
			x += movement;
			playerRight.getScaledCopy(20, 20).draw(this.x - 10, this.y - 10);

		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT) && (x - 10 > 0)) {
			x -= movement;
			playerLeft.getScaledCopy(20, 20).draw(this.x - 10, this.y - 10);

		}

		circ = new Circle(x, y, 10);
		counter++;

	}

	public void draw(Graphics g) {
		playerUp.getScaledCopy(20, 20).draw(this.x - 10, this.y - 10);
		if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
			if (counter < counterMax / 2) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
					playerDownLeft.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
					playerDown.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}

			} else if (counter > counterMax / 2 && counter < counterMax) {
				if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
					playerDownLeft.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
					playerDown2.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax) {
				counter = 0;
			}
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
			if (counter < counterMax / 2) {
				if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
					playerUpRight.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
					playerUp.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax / 2 && counter < counterMax) {
				if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
					playerUpRight.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
					playerUp2.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax) {
				counter = 0;
			}
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			if (counter < counterMax / 2) {
				if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
					playerDownRight.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_UP)) {
					playerRight.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax / 2 && counter < counterMax) {
				if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
					playerDownRight.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_UP)) {
					playerRight2.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax) {
				counter = 0;
			}
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			if (counter < counterMax / 2) {
				if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
					playerUpLeft.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
					playerLeft.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax / 2 && counter < counterMax) {
				if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
					playerUpLeft.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				} else if (!Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
					playerLeft2.getScaledCopy(20, 20).draw(this.x - 10,
							this.y - 10);
				}
			} else if (counter > counterMax) {
				counter = 0;
			}
		}
	}

}
