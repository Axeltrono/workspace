package topdown;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class TDMain extends BasicGame{

	public TDMain(String title) {
		super("poop");
		// TODO Auto-generated constructor stub
	}
	
	MainCharacter mainChar;
	ArrayList<Bullet> bulletsOffScreen;
	ArrayList<Enemy> enemies;
	ArrayList<Enemy> deadEnemies;
	Audio music;
	int countDownTimer = 250;
	int score = 0;
	int health = 2;
	String time;
	Random r;
	int maxBros = 10;
	Audio deathMusic;
	TDBackground bg;
	TDBackground bg2;
	
	boolean hasBeenPlayed = false;
	
	
	@Override
	public void init(GameContainer container) throws SlickException {
		bg = new TDBackground(container, 0, 0);
		bg2 = new TDBackground(container, 0, -600);
		// TODO Auto-generated method stub
		try {
			mainChar = new MainCharacter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		try {
			deathMusic = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("shadowgate.wav"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("castlevania.wav"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		music.playAsMusic(1.0f, 1.0f, true);
		bulletsOffScreen = new ArrayList<Bullet>();
		enemies = new ArrayList<Enemy>();
		r = new Random();
		deadEnemies = new ArrayList<Enemy>();
		
		for (int i = 0; i <= maxBros; i++) {
			try {
				enemies.add(new Enemy(50 + r.nextInt(500), r.nextInt(50) - 25, health));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//enemy = new Enemy(300, 0, 300);
		
		
	}

	@Override
	public void render(GameContainer container, Graphics g)	throws SlickException {
		bg.scrollY();
		bg2.scrollY();
		if (bg.patternPositionY >= 600) {
			bg.patternPositionY = -600;
		}
		if (bg2.patternPositionY >= 600) {
			bg2.patternPositionY = -600;
		}
		
		
		
		mainChar.render(g);
		for(Enemy enemy : enemies){
			if ((enemy.getDistance(mainChar.x, mainChar.y) <= 15 && !enemy.isDead) || enemy.y > 600) {
				mainChar.isDead = true;
				if (!hasBeenPlayed) {
					deathMusic.playAsMusic(1.0f, 1.0f, true);
					mainChar.playSoundEffect();
					hasBeenPlayed = true;
				}
			}
			enemy.render(g);
		}
		
		for (Enemy enemy : enemies){
			if (enemy.isDead) {
				deadEnemies.add(enemy);
			}
		}
		
		enemies.removeAll(deadEnemies);
		g.drawString("SCORE: " + score, 0, 550);
		if (mainChar.isDead) {
			g.drawString("YOU DIED", 260, 200);

		}
		if (enemies.isEmpty()) {
			g.drawString("YOU WIN", 50, 100);			
			if (countDownTimer % 50 == 0) {
				time = "" + (countDownTimer / 50);
			}
			g.drawString(time, 300, 100);
			countDownTimer--;
		}
		
		if (countDownTimer == 0) {
			//health++;			
			for (int i = 0; i <= maxBros; i++) {
				try {
					enemies.add(new Enemy(50 + r.nextInt(500), r.nextInt(50) - 25, health));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			countDownTimer = 250;			
		}

		for (Bullet bullet : mainChar.bullets) {
			if ((bullet.y > 600 || bullet.y < 0) || (bullet.x > 600 || bullet.x < 0)) {
				bulletsOffScreen.add(bullet);
			}
			for (Enemy enemy : enemies){
				if (enemy.getDistance(bullet.x, bullet.y) <= 13 && !enemy.isDead) {
					enemy.playHurtSound();
					enemy.health--;
					if (enemy.health < 0) {
						score += health * 10;
						enemy.playDeathSound();
						enemy.isDead = true;
					}
					bulletsOffScreen.add(bullet);
				}
			}
			
		}
		mainChar.bullets.removeAll(bulletsOffScreen);
	}

	

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		for (Enemy enemy : enemies){
			enemy.update();
		}
		mainChar.update();
		
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new TDMain("Top Down"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
