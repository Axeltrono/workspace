package gravity;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;

public class Floor {
	int x;
	int y;
	Rectangle rect;
	Image floorBox;
	public boolean isMoving = true;
	public Floor(int x, int y) throws SlickException{
		floorBox = new Image("floor.png");
		this.x = x;
		this.y = y;
		rect = new Rectangle(x, y, 600, 25);
	}
	
	public void update(){
		if(isMoving){
			x-=2;
		}
		if (x + 600 <= 0) {
			x = 600;
		}
	}
	
	public void draw(Graphics g){
		floorBox.draw(x, y);
		//g.draw(rect);
	}

}
