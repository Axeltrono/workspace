package gravity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class GravityMain extends BasicGame{

	public GravityMain(String title) {
		super(title);
	}
	
	LoseMenu loser;
	BouncyBall ball;
	Pipes pipe;
	Pipes pipe2;
	int highscore;
	FileMain handler;
	Audio music;
	Background bg;
	Background bg2;
	Image quote;
	boolean rHasBeenPressed = true;
	Floor floor1;
	Floor floor2;
	
	@Override
	public void init(GameContainer container) throws SlickException{
		floor1 = new Floor(0, 575);
		floor2 = new Floor(600, 575);
		bg = new Background(container, 0, 0);
		bg2 = new Background(container, 600, 0);
		loser = new LoseMenu();
		quote = new Image("lilbheadswag.png");
		ball = new BouncyBall(this);
		
		try {
			pipe = new Pipes(1000);
			pipe2 = new Pipes(650);
			music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("megaman.wav"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		music.playAsMusic(1.0f, 1.0f, true);
		handler = new FileMain("highball.score.txt");
		try {
			highscore = handler.read(new File("highscore.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		// set background to white
		g.setColor(Color.white);
		bg.scrollX();
		bg2.scrollX();
		
		
		if (bg.patternPositionX + 600 <= 0) {
			bg.patternPositionX = 600;
		}
		if (bg2.patternPositionX + 600 <= 0) {
			bg2.patternPositionX = 600;
		}
		floor1.draw(g);
		floor2.draw(g);
		pipe.render(g);
		pipe2.render(g);
		ball.render(g);
		
		if (ball.isBallDead) {
			loser.render(g);
			pipe.isMoving = false;
			pipe2.isMoving = false;
			floor1.isMoving = false;
			floor2.isMoving = false;
			bg.isMoving = false;
			bg2.isMoving = false;
			quote.getScaledCopy(150, 127).draw(200, 130);
			if (ball.score == 1) {
				g.drawString("RANK: INFANT", 200, 300);
			} else if (ball.score > 1 && ball.score <= 7){
				g.drawString("RANK: NOVICE", 200, 300);
			} else if (ball.score > 7 && ball.score <= 13){
				g.drawString("RANK: APPRENTICE", 200, 300);
			} else if (ball.score > 13 && ball.score <= 20){
				g.drawString("RANK: JOURNEYMAN", 200, 300);
			} else if (ball.score > 20 && ball.score <= 35){
				g.drawString("RANK: MASTER", 200, 300);
			} else if (ball.score > 35 && ball.score <= 50){
				g.drawString("RANK: GOD", 200, 300);
			} else if (ball.score > 50 && ball.score <= 99){
				g.drawString("RANK: BASED GOD", 200, 300);
			} else if(ball.score > 100){
				g.drawString("RANK: THAT IS MEAN", 200, 300);
			}
			else {
				g.drawString("RANK: BAD", 200, 300);
			}
			g.drawString("SCORE: " + ball.score, 200, 350);
			g.drawString("HIGH SCORE: " + highscore, 200, 100);
		}
		
		if (rHasBeenPressed) {
			g.drawString("PRESS UP", 250, 100);
		}
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		floor1.update();
		floor2.update();
		ball.update(delta);
		pipe.update(ball);
		pipe2.update(ball);
		if (ball.score > highscore) {
			highscore = ball.score;
			handler.write("" + highscore);	
		}
		
		if (Keyboard.isKeyDown(Keyboard.KEY_R) || rHasBeenPressed) {
			ball.isBallDead = false;
			rHasBeenPressed = true;
			pipe.isMoving = true;
			pipe2.isMoving = true;
			floor1.isMoving = true;
			floor2.isMoving = true;
			bg.isMoving = true;
			bg2.isMoving = true;
			ball.score = 0;
			pipe.x = pipe.startingX + 20;
			pipe2.x = pipe2.startingX + 20;
			pipe.height = pipe.r.nextInt(450);
			pipe2.height = pipe2.r.nextInt(450);
			ball.y = 100;
			ball.x = 200;
			ball.velocity = 0;
			if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				rHasBeenPressed = false;
			}
		}
	}
	
	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new GravityMain("BASEY GOD"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
