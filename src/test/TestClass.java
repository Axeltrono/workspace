package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

import pong.PongMain;
import pong.RightPaddle;

public class TestClass extends BasicGame{
	Ball ball;
	ArrayList<Platform> platforms = new ArrayList<Platform>();
	int level = 1;
	Blocky block;
	int coinsGot;
	Audio music;
	Image background;
	int TOTAL_COINS = 10;
	TrueTypeFont font;
	int countDownTimer = 250;
	String time;
	float pitch = 1.0f;
	boolean level1colorer = true;
	
	int test = 0;
	boolean debug = false;
	
	BadGuy pitbull;
	
	
	ArrayList<Coin> coins;
	public ArrayList<Coin> coinsGotten = new ArrayList<Coin>();
	Random rand = new Random();
	
	public TestClass(String title) {
		super(title);
	}
	
	@Override
	public void init(GameContainer gc) throws SlickException{
		try {
			music = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("overworld.wav"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try {
			ball = new Ball(30, 40);
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		block = new Blocky(20, 20, 300, 300);
		platforms.add(new Platform(570, 570, 25, 50));
		for (int i = 0; i < 10; i++) {
			platforms.add(new Platform(rand.nextInt(600), 60 * i, 25, 25));
		}
		try {
			pitbull = new BadGuy(rand.nextInt(600), rand.nextInt(600));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		music.playAsMusic(pitch, 1.0f, true);
		coins = new ArrayList<Coin>();
		
		
		for (int i = 0; i < TOTAL_COINS; i++) {
			try {
				coins.add(new Coin(rand.nextInt(600), rand.nextInt(600)));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		ball.draw(g);
		//pitbull.draw(g);
		for(Platform plat : platforms){
			plat.draw(g);
		}
		if (pitbull.hasBeenGot) {
			//System.exit(123);
		}
		
		
		
		for (Coin coin: coins){
			if(!coin.hasBeenGot){
				coin.draw(g);
			} else {
				coinsGotten.add(coin);
				coinsGot++;
			}
			
		}
		
		if (level == 1 && level1colorer) {
			g.setBackground(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
			level1colorer = false;
		}
		
		if (coinsGot < TOTAL_COINS){
			g.drawString("COINS:" + coinsGot, 50, 100);
			g.drawString("LEVEL: " + level, 200, 100);
		} else {
			g.drawString("YOU WIN!", 50, 100);
			if (countDownTimer % 50 == 0) {
				time = "" + (countDownTimer / 50);
			}
			g.drawString(time, 300, 100);
			countDownTimer--;
		}
		if(countDownTimer == 0){
			coinsGotten.clear();
			g.setBackground(new Color(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), rand.nextFloat()));
			pitbull.x = rand.nextInt(600);
			pitbull.y = rand.nextInt(600);
			pitbull.counter = 0;
			level++;
			int holder = ball.movement;
			if(level % 5 == 0){
				g.drawString("WHOAH MAMA!!!", 300, 300);
				ball.movement = 50;
			} else {
				ball.movement = holder;
				ball.movement += 3;
				
			}
			test += 5;
			ball.counterMax-=2;
			pitch+= .1;
			music.playAsMusic(pitch, 1.0f, true);
			//TOTAL_COINS += test;
			for (int i = 0; i < TOTAL_COINS; i++) {
					try {
						coins.add(new Coin(rand.nextInt(600), rand.nextInt(600)));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			countDownTimer = 250;
			coinsGot = 0;
		}
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		ball.move(delta);
		//pitbull.move(delta);
		//pitbull.update(ball);
		ball.jumper = coinsGotten.size() + 1;
		
		
		
		
		
		
		for (Coin coin : coins) {
			coin.update(ball);
			if (pitch >= 1.3f) {
				coin.move(delta);
				
			}
			
		}
		
		coins.removeAll(coinsGotten);
		for(Platform plat : platforms){
			if (plat.update(ball)) {
				break;
			}
		}
		
		
		
	}
	
	   

	public static void main(String[] args) {
		try {
			AppGameContainer window = new AppGameContainer(new TestClass("movin round"));
			window.setDisplayMode(600, 600, false);
			window.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
