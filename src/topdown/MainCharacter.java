package topdown;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class MainCharacter {
	int x;
	int y;
	Circle circle;
	Bullet bullet;
	ArrayList<Bullet> bullets;
	Random r;
	boolean isDead = false;
	int shootCounter;
	int coolDown = 4;
	Audio shot;
	Audio playerdeath;
	
	
	public MainCharacter() throws IOException{
		x = 300;
		y = 300;
		circle = new Circle(x, y, 5);
		bullet = new Bullet("down", 800, 800);
		r = new Random();
		bullets = new ArrayList<Bullet>();
		shot = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("shortshot.wav"));
		playerdeath = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("playerdeath.wav"));


	}
	
	public void update(){
		
		if (!isDead){

			if (Keyboard.isKeyDown(Keyboard.KEY_W) && y - 5 > 0) {
				y-=3;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_S) && y + 5 < 600) {
				y+=3;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_D) && x + 5 < 600) {
				x+=3;
			}
			if (Keyboard.isKeyDown(Keyboard.KEY_A) && x - 5 > 0) {
				x-=3;
			}
			
			if (Keyboard.isKeyDown(Keyboard.KEY_UP)) {
				if (shootCounter > coolDown) {
					shot.playAsSoundEffect(1.0f, 1.0f, false);
					bullets.add(new Bullet("up", x, y));
					shootCounter = 0;
				}
			} /*else if (Keyboard.isKeyDown(Keyboard.KEY_DOWN)) {
				if (shootCounter > coolDown) {
					shot.playAsSoundEffect(1.0f, 1.0f, false);
					bullets.add(new Bullet("down", x, y));
					shootCounter = 0;
				}			
			} else if (Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
				if (shootCounter > coolDown) {
					shot.playAsSoundEffect(1.0f, 1.0f, false);
					bullets.add(new Bullet("left", x, y));
					shootCounter = 0;
				}
			} else if (Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
				if (shootCounter > coolDown) {
					shot.playAsSoundEffect(1.0f, 1.0f, false);
					bullets.add(new Bullet("right", x, y));
					shootCounter = 0;
				}
			}*/
			//bullet.update();
			for (Bullet bullet : bullets) {
				bullet.update();
			}
			shootCounter++;
			circle = new Circle(x, y, 5);
		
		}
	}
	
	public void render(Graphics g){
		if (!isDead){
			g.draw(circle);
			for (Bullet bullet : bullets) {
				bullet.render(g);
			}
		}
		//bullet.render(g);
	}
	
	public void playSoundEffect(){
		playerdeath.playAsSoundEffect(1.0f, 1.0f, false);
	}
	

}
