package electricwave;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;

public class WaveBall {
	int ypix;
	float x;
	float y;
	float z;
	float s = 10;
	float swag = -61.19455534446f;
	float r = 30;
	float b = 8 / 3; 
	Circle circle;
	float vx;
	float vy;
	float vz;
	int xpix = 0;
	
	public WaveBall(float x, float y, float z){
		this.x = x;
		this.y = y;
		this.z = z;
		circle = new Circle(x, y, 3);
	}
	
	public void update(int delta){
		vx = s * (y - x);
		vy = x * (r - z) - y;
		vz = (x * y - b * z);
		x+=(vx * (delta/1000f));
		y+=(vy * (delta/1000f));
		z+=(vz * (delta/1000f));
		ypix++;
		circle = new Circle(5*x + 300, 5*z + 300, y);
	}
	
	public void draw(Graphics g){
		g.draw(circle);
	}
}
