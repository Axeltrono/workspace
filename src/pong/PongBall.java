package pong;

import java.io.IOException;
import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.util.ResourceLoader;

public class PongBall {
	Circle circle;
	int x;
	int y;
	int velocityX = 3;
	int velocityY = 3;
	int player1Score = 0;
	int player2Score = 0;
	Image pitbull;
	Audio donk;
	Audio dink;
	Audio beep;
	Random r = new Random();
	
	public PongBall(int x, int y) throws SlickException, IOException{
		pitbull = new Image("pitbull.png");
		donk = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("ping_pong_8bit_plop.wav"));
		dink = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("ping_pong_8bit_beeep.wav"));
		beep = AudioLoader.getAudio("WAV", ResourceLoader.getResourceAsStream("ping_pong_8bit_peeeeeep.wav"));
		this.x = x;
		this.y = y;
		circle = new Circle(x, y, 15);
	}
	
	public boolean update(LeftPaddle lpad, RightPaddle rpad){
		x+=velocityX;
		y+=velocityY;
		
		if (x <= 0) {
			player2Score++;
			x = 400;
			y = 300;
			velocityX = 3;
			lpad.velocity = 5;
			rpad.velocity = 5;
			lpad.height = 100;
			rpad.height = 100;
			beep.playAsSoundEffect(1.0f, 1.0f, false);
			return true;
		}
		if (x >= 800) {
			player1Score++;
			x = 400;
			y = 300;
			velocityX = -3;
			lpad.velocity = 5;
			rpad.velocity = 5;
			lpad.height = 100;
			rpad.height = 100;
			beep.playAsSoundEffect(1.0f, 1.0f, false);
			return true;
		}
		if ((y > rpad.y - 10 && y < rpad.y + rpad.height + 10) && x + 15 > 775) {
			velocityX = -velocityX;
			lpad.velocity++;
			rpad.velocity++;
			donk.playAsSoundEffect(1.0f, 1.0f, false);
			velocityX--;
			rpad.height-=2;
			lpad.height-=2;
			System.out.println("doink");
		}
		if ((y > lpad.y - 10 && y < lpad.y + lpad.height + 10) && x - 15 < 25) {
			velocityX = -velocityX;
			lpad.velocity++;
			rpad.velocity++;
			rpad.height-=2;
			lpad.height-=2;
			dink.playAsSoundEffect(1.0f, 1.0f, false);
			velocityX++;
			System.out.println("boink");
		}
		if (y + 15 >= 600) {
			velocityY = -velocityY;
		}
		if (y - 15 <= 0) {
			velocityY = -velocityY;
		}
		circle = new Circle(x, y, 15);
		return false;
	}
	
	
	public boolean update(LeftPaddle lpad, ComputerPaddle rpad){
		x+=velocityX;
		y+=velocityY;	
		
		if (x <= 0) {
			player2Score++;
			x = 400;
			y = 300;
			velocityX = 3;
			lpad.velocity = 5;
			rpad.velocity = 5;
			lpad.height = 100;
			rpad.height = 100;
			beep.playAsSoundEffect(1.0f, 1.0f, false);
			return true;
		}
		if (x >= 800) {
			player1Score++;
			x = 400;
			y = 300;
			velocityX = -3;
			lpad.velocity = 5;
			rpad.velocity = 5;
			lpad.height = 100;
			rpad.height = 100;
			beep.playAsSoundEffect(1.0f, 1.0f, false);
			return true;
		}
		if ((y > rpad.y - 10 && y < rpad.y + rpad.height + 10) && x + 15 > 775) {
			velocityX = -velocityX;
			lpad.velocity++;
			rpad.velocity++;
			donk.playAsSoundEffect(1.0f, 1.0f, false);
			//velocityX--;
			rpad.height-=2;
			lpad.height-=2;
			System.out.println("doink");
		}
		if ((y > lpad.y - 10 && y < lpad.y + lpad.height + 10) && x - 15 < 25) {
			velocityX = -velocityX;
			lpad.velocity++;
			rpad.velocity++;
			rpad.height-=2;
			lpad.height-=2;
			dink.playAsSoundEffect(1.0f, 1.0f, false);
			//velocityX++;
			System.out.println("boink");
		}
		if (y + 15 >= 600) {
			velocityY = -velocityY;
		}
		if (y - 15 <= 0) {
			velocityY = -velocityY;
		}
		circle = new Circle(x, y, 15);
		return false;
	}
	
	
	public void draw(Graphics g){
		//pitbull.getScaledCopy(30, 30).draw(x - 15, y - 15);
		g.draw(circle);
	}

}
